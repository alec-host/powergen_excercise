package main

import (
	//"coding-test/data" // commented this path out and replaced with "powergen/data" path in the next line

	"context"
	"log"
	"net/http"
	"powergen/data"

	//config "powergen/billing/payment-gateway/appconfig" // commented this path out and replaced with "powergen/appconfig" path defined in the next line

	config "powergen/appconfig"

	"github.com/gorilla/mux"
)

type ContextInjector struct {
	ctx context.Context
	h   http.HandlerFunc
}

func (ci *ContextInjector) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	ci.h.ServeHTTP(w, r.WithContext(ci.ctx))
}

func main() {

	conf := buildConfigs()

	// connect to database
	err := data.Connect("postgres", conf.PaymentDb_Conn)
	if err != nil {
		log.Fatalf("Failed to connect to postgres")
		return
	}
	defer data.DB.Close()

	// initialize routes
	router := mux.NewRouter()
	//initializeRoutes(router)

	// start server
	log.Fatal(http.ListenAndServe(":9009", router).Error(), nil)
}

func buildConfigs() config.AppConfig {
	var conf config.AppConfig
	conf = config.AppConfig{
		PaymentDb_Conn: "host=coding-test.powergen-re.net port=5432 user=postgres password=a01PYeu#&5GpjJk dbname=billing sslmode=disable",
	}
	config.SetConfig(conf)

	return conf
}

/*
moved here for testing .
func initializeRoutes(router *mux.Router) {
	router.HandleFunc("/payment_gateway/api/receive", payments.Receive)
	router.HandleFunc("/payment_gateway/api/fetch", payments.Fetch)
}
*/
