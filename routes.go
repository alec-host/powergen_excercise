package main

import (
	
	"powergen/payments"

	"github.com/gorilla/mux"
)

func initializeRoutes(router *mux.Router) {
	router.HandleFunc("/payment_gateway/api/receive", payments.Receive)
	router.HandleFunc("/payment_gateway/api/fetch", payments.Fetch)
}
