package appconfig

type AppConfig struct {
	PaymentDb_Conn        string
	MeteringServ_Endpoint string
}

var _configs AppConfig

func SetConfig(conf AppConfig) {
	_configs = conf
}

func GetConfig() AppConfig {
	return _configs
}
