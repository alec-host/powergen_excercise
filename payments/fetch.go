package payments

import (
	"encoding/json"
	"fmt"
	"net/http"
	"powergen/data"
	"time"
)

/*
	1. Write the Fetch function
	Fetch should take a request with query parameters start_date and end_date and
	return all payments made between start_date and end_date. Both start_date and
	end_date should be optional parameters, where start_date defaults to one month
	from now, and end_date defaults to now. Your function should return a JSON array
	of PaymentsView objects. HINT - check out the Scan function in GORM

	start_date is the earlier date, and end_date is the later date
*/

/*
	DateParam: define struct to provide optional ability
*/
type DateParams struct {
	startDate, endDate time.Time
}

type PaymentsView struct {
	Amount        float64 `gorm:"column:amount"`
	AccountNumber string  `gorm:"column:account_no"`
	PhoneNumber   string  `gorm:"column:phone_number"`
	PaymentDate   string  `gorm:"column:date_paid"`
	Reference     string  `gorm:"column:mno_ref"`
}

/*
	result - user defined collection of fields
*/
type Result struct {
	AccountID          string
	AccountNumber      string
	DatePaid           *time.Time
	DateLastUpdatedUTC *time.Time
}

/*
	json - user defined collection of fields
*/
type myJSON struct {
	Array []string
}

func Fetch(w http.ResponseWriter, r *http.Request) {
	var paymentView PaymentsView
	//Get the payment body and decode it
	err := json.NewDecoder(r.Body).Decode(&paymentView)
	if err != nil {
		clientError(w, http.StatusBadRequest, ErrorParseJSONBody)
		return
	}

	var inputDateFilter DateParams

	/*
		routine call to select data from db.
	*/
	qryPowerBillingInfo(w, inputDateFilter)
}

/*
	select payment records
*/
func qryPowerBillingInfo(w http.ResponseWriter, pickedDate DateParams) {
	var result Result

	/*
		check whether filter inputs are set.
	*/
	var qryFilterOne = len(pickedDate.startDate.String())
	var qryFilterTwo = len(pickedDate.startDate.String())

	if qryFilterOne == 0 {
		/*
			get previous month if not set.
		*/
		pickedDate.startDate = time.Now().AddDate(0, -1, 0)
	}

	if qryFilterTwo == 0 {
		/*
			get current month if not set
		*/
		pickedDate.endDate = time.Now()
	}

	// retrieve records.
	data.DB.Raw("SELECT account_id,account_no,date_paid,date_last_updated_utc FROM payment.payments WHERE date_paid BETWEEN ", pickedDate.startDate, " AND ", pickedDate.endDate).Scan(&result)

	/*
		convert to json array.
	*/
	jsondat := &myJSON{Array: []string{result.AccountID, result.AccountNumber, result.DatePaid.String(), result.DateLastUpdatedUTC.String()}}

	encjson, _ := json.Marshal(jsondat)

	/*
		print output.
	*/
	fmt.Println(string(encjson))
}
