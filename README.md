# PowerGen-RE Coding challenge

Welcome to the PowerGen coding challenge! We've put together a short coding assignment that will allow us to better asses your programming skills. We'd like you to write a small microservice in Golang with two endpoints, one to accept and save payments to the DB and another to fetch those payments from the database. The assignment should take you between 4 and 6 hours to complete. Before you get going, we want to lay out what we're looking for, so you can focus on what's important.

## What We're Evaluating You On

1. First, we're **not** testing your ability to code in Go. We chose Golang because it's what we use internally, because it has fairly simple syntax, and because the language is pretty minimal, so already knowing it won't give you much of an advantage. We also aren't expecting our candidates to have programmed in Go before, so don't worry if you haven't!

sooo, what are we testing?

1. Does your code work? We'll be testing both success and failure cases, but don't worry about really weird cases. We're not trying to trick you
1. Your ability to learn new things quickly. We expect our senior software engineers to be language agnostic, and be able to pick up new tech quickly. If you need to write a script in python, or spin up a Java app because there's a library that will do the job better than Go, we need you to be able to do that
1. Your ability to work off other people's code. As a software engineer you're constantly working on code that other people have written. We expect you to be able to jump into a repo, understand what's already there, and effectively use the code that others have written. This means that you'll be evaluated partly on how well you use the code that we've written for you.
1. The logic and flow of the app. Does your code make sense? Have you written readable, clean, modular and/or reusable functions? Avoid writting needlessly complicated code, or doing overly fancy tricks.
1. Your git workflow. Commit early and commit often. We're not looking for anything fancy here, just that your commits make sense and you're not just pushing one giant commit when you're done the challenge.


## The Challenge

You have two primary tasks:
1. Finish writing the Receive function in payments/receive.go
1. Write the Fetch function in payments/fetch.go

When you're done, you'll submit:
1. Your source code
2. While you're coding, note down where you ran into trouble. We'll go through it when you come in for an in-person interview. This doesn't have to be very detailed, but we're interested in your problem solving process

#### Before you start coding

Fork this repo. You'll only be given read privleges, so you'll need to work off of your own repo

[Download Go](https://golang.org/dl/)

You can code in Go in number of different development environments. We use [Visual Studio Code](https://code.visualstudio.com/), but you can also download a free trial of [GoLand](https://www.jetbrains.com/go/), or just use a text editor such as [Submlime Text](https://www.sublimetext.com/). 

Make sure your [GOPATH](https://golang.org/cmd/go/#hdr-GOPATH_environment_variable) and [GOROOT](http://golang.org/doc/install#tarball_non_standard) environment variables are [set correctly](https://stackoverflow.com/questions/7970390/what-should-be-the-values-of-gopath-and-goroot)

For any of these options, you can run your go program from the command line using `go run main.go`. In Visual Studio Code, you can run your program in debug mode (somewhat unintuitively) using F5 (you have to have main.go as your active window in VS Code). You'll be able to run the program as soon as you download it. We recommend playing around a bit before you start talking the challenge

#### When you start coding

1. Start with payments/receive.go There are three places where you need to fill in functionality. All are well commented. 

2. When you're done, write the Fetch function in payments/fetch.go. Full instructions are in the comments in the file

**Use account number 12345-01 for all tests**


#### Some useful links

1. [GORM](https://gorm.io/) - You'll need to use GORM for performing DB queries. I've found db.LogMode(true) to be particularly helpful for query debugging 
2. Here's a [Go CheatSheet](https://devhints.io/go) for getting the basics really quickly
3. And here's a more [in-depth tutorial](https://tour.golang.org/welcome/1)


#### After you're done.

1. Make sure all of your code has been pushed to the repo you've been working in. We'll pull it and run it locally.


### If you have any questions

Feel free to reach out! When you're working at PowerGen you'll be part of a team, so we're happy to help.


