package models

import (
	"time"

	"github.com/google/uuid"
)

/*
	ParseStringToUUID takes a string and returns a UUID version of the unique identifier
*/
func ParseStringToUUID(str string) (uuid.UUID, error) {
	return uuid.Parse(str)
}

/*
	NewUUID returns a unique identifier
*/
func NewUUID() uuid.UUID {
	return uuid.New()
}

/*
	Database table: payment.accounts
*/
type Account struct {
	Id                 uuid.UUID  `gorm:"column:id"`
	AccountNo          string     `gorm:"column:account_no"`
	DateCreatedUTC     time.Time  `gorm:"column:date_created_utc"`
	DateLastUpdatedUTC *time.Time `gorm:"column:date_last_updated_utc"`
}

func (Account) TableName() string {
	return "payment.accounts"
}

/*
	Database table: payment.payments
*/
type Payment struct {
	ID                 uuid.UUID  `gorm:"column:id"`
	AccountID          uuid.UUID  `gorm:"column:account_id"`
	AccountNo          string     `gorm:"column:account_no"`
	Amount             float64    `gorm:"column:amount"`
	DatePaid           *time.Time `gorm:"column:date_paid"`
	DateLastUpdatedUTC *time.Time `gorm:"column:date_last_updated_utc"`
}

/*
	TableName() returns the name of the payments table
*/
func (Payment) TableName() string {
	return "payment.payments"
}

/*
	Database table: payment.mobile_money_payments
*/
type MobileMoneyPayment struct {
	ID                 uuid.UUID  `gorm:"column:id"`
	PaymentID          *uuid.UUID `gorm:"column:payment_id"`
	Payment            Payment    `gorm:"association_foreignkey:payment_id"` //Nested Payment. HINT FOR FETCH - you can take advantage of this
	PhoneNumber        string     `gorm:"column:phone_number"`
	MNO                string     `gorm:"column:mno"`
	MNORef             string     `gorm:"column:mno_ref"`
	DateCreatedUTC     time.Time  `gorm:"column:date_created_utc"`
	DateLastUpdatedUTC *time.Time `gorm:"column:date_last_updated_utc"`
}

func (MobileMoneyPayment) TableName() string {
	return "payment.mobile_money_payments"
}
