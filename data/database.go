package data

import (
	"log"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

// DB is a pointer to the GORM db object. Use this to query
var DB *gorm.DB

//var country *public.CountryInstance

/*
	Connect to a database using dialect (type of DB) dialect, and
	connection string connstr
*/

func Connect(dialect, connstr string) error {
	db, err := gorm.Open(dialect, connstr)
	if err != nil {
		//kkkk
		log.Fatal(err.Error(), connstr)
	}
	db.LogMode(true)
	DB = db
	return err
}
